# dwmstatus

This is my fork of dwmstatus.

## description

Barebone status monitor with basic functions written in C. This follows the suckless philosophy, to give you an easy way to extend the source code to your needs. See the helper functions for C below, to extend it to your needs. Just check it out and keep on hacking.

## features

- cpu temperature
- battery percentage based on last full capacity
- date and time

## usage

```
git clone git://git.suckless.org/dwmstatus
cd dwmstatus
make
make PREFIX=/usr install
```

then add `dwmstatus 2>&1 >/dev/null &` to your .xinitrc
